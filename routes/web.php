<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;

Route::get('/', function () {
    return view('welcome');
});

//product route
Route::get('/create-product',  [ProductController::class,'create_product']);
Route::post('/add-product',    [ProductController::class,'store'])->name('add.product');
Route::get('/delete-product',  [ProductController::class,'delete'])->name('delete.product');
Route::get('/get-subproduct',  [ProductController::class,'get_subproduct']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
