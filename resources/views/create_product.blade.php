@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>
                <!-- Button trigger modal -->
                <a href="{{ route('home') }}" class="btn btn-primary">Manage Product </a>
                <div class="card-body">

                    <div class="mydata">
                        <form action="{{ route('add.product') }}" method="post" id="addProductForm" enctype="multipart/form-data">
                        @csrf
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Title</label>
                                    <input type="text" name="title" id="title" class="form-control" >
                                    @error('title')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Sub-Category</label>
                                    <select name="subcategory_id" id="subcategory_id" class="form-control">
                                        <option value="">Please Select</option>
                                        @foreach($get_all as $value)
                                            <option value="{{ $value->id }}">{{ $value->subcategory_name }}</option>
                                        @endforeach
                                    </select>
                                    @error('subcategory_id')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>  
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Description</label>
                                    <textarea name="description" id="description" class="form-control ckeditor"></textarea>
                                    @error('description')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Price</label>
                                    <input type="text" name="price" id="price" class="form-control">
                                    @error('price')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="name">Thumbnail</label>
                                    <input type="file" name="thumbnail" id="thumbnail" class="form-control">
                                    @error('thumbnail')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection