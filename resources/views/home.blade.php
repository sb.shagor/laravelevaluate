@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>
                <!-- Button trigger modal -->
                <a href="{{ url('create-product') }}" class="btn btn-primary">Add Product </a>

                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4">Filter by:</div>
                        <div class="col-md-6">
                            <select name='subcategory_id' id="subcategory_id"  class="form-control">
                                <option value=''>Select Sub-Category</option>
                                @foreach($get_all_subcategories as $value)
                                    <option value="{{ $value->id }}">{{ $value->subcategory_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="mydata">
                        <table class="table table-bordered" id="example">
                          <thead>
                            <tr>
                              <th scope="col">SL</th>
                              <th scope="col">Product</th>
                              <th scope="col">Sub Category</th>
                              <th scope="col">Price</th>
                              <th scope="col">Description</th>
                              <th scope="col">Thumbnail</th>
                              <th scope="col">Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach($get_all as $key => $value)
                                <tr>
                                  <th scope="row">{{ ++$key }}</th>
                                  <td>{{ $value->title }}</td>
                                  <td>{{ $value->subcategories->subcategory_name }}</td>
                                  <td>{{ $value->price }}</td>
                                  <td>{!! $value->description !!}</td>
                                  <td>
                                    <a href="{{ asset($value->thumbnail) }}" target="_blank"><i class="las la-eye btn btn-primary btn-sm"></i></a></td>
                                  <td>

                                    <a href="#" id="{{$value->id}}" class="btn btn-danger delete_product btn-sm"><i class="las la-times"></i></a></td>
                                </tr>
                            @endforeach
                          </tbody>
                        </table>
                    </div>
                </div>

                <div class="card-body">
                    <div class="mydata" id="ajaxDataTable" style="display: none;">
                        
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
