<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.3/dist/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){

        $(document).on('change','#subcategory_id',function(){
            var subcategory_id = $(this).val();
            $.ajax({
                url:"{{ url('get-subproduct') }}",
                data:{subcategory_id:subcategory_id},
                type: "GET",
                success:function(data) {
                    $('#ajaxDataTable').show();
                    $('#example').hide();
                    $('#ajaxDataTable').html(data.html);
                }
            });
        });

        $(document).on('click','.delete_product',function(e){
            e.preventDefault();
            var id = $(this).attr('id');

            if (!confirm('Sure to delete?')) {
                return;
            }

            $.ajax({
                url:"{{ route('delete.product') }}",
                method:'get',
                data:{id:id},
                success:function(res){
                    if(res.status=='success'){   
                        swal("Congratulations","Successfully Deleted","success",{
                            button:"ok"
                        });             
                        $('.mydata').load(location.href+' .mydata');
                    }
                },error:function(err){
                    alert('Error');
                }
            });
        });
    });
</script>