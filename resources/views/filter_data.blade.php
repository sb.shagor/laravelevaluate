<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">SL</th>
      <th scope="col">Product</th>
      <th scope="col">Sub Category</th>
      <th scope="col">Price</th>
      <th scope="col">Description</th>
      <th scope="col">Thumbnail</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @foreach($filter_data as $key => $value)
        <tr>
          <th scope="row">{{ ++$key }}</th>
          <td>{{ $value->title }}</td>
          <td>{{ $value->subcategories->subcategory_name }}</td>
          <td>{{ $value->price }}</td>
          <td>{!! $value->description !!}</td>
          <td>
            <a href="{{ asset($value->thumbnail) }}" target="_blank"><i class="las la-eye btn btn-primary btn-sm"></i></a></td>
          <td>

            <a href="#" id="{{$value->id}}" class="btn btn-danger delete_product btn-sm"><i class="las la-times"></i></a></td>
        </tr>
    @endforeach
  </tbody>
</table>