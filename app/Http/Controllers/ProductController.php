<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\SubCategory;
use Session;

class ProductController extends Controller{

    public function create_product(){
        $data['get_all'] = SubCategory::latest()->get();
        return view('create_product',$data);
    }
    
    public function store(Request $request){
        $request->validate([
                'title'          => 'required|unique:products',
                'subcategory_id' => 'required',
                'description'    => 'required',
                'thumbnail'      => 'required',
                'price'          => 'required'
            ],
            [
                'title.required'          =>'Product Title is Required',
                'title.unique'            =>'This Product is Added',
                'subcategory_id.required' =>'Sub Category is Required',
                'description.required'    =>'Description is Required',
                'thumbnail.required'      =>'Thumbnail is Required',
                'price.required'          =>'Price is Required',
            ]
        );
        
        $product = new Product();
        $product->title = $request->title;
        $product->subcategory_id = $request->subcategory_id;
        $product->description = $request->description;
        $product->price = $request->price;

        if(!empty($request->file('thumbnail'))){
            $File = $request->file('thumbnail');
            $name = $File->getClientOriginalName();
            $ext = explode('.',$name);
            $finalName = 'product-'.time().'.'.$ext[1];
            $uploadPath = 'uploads/';
            $File->move($uploadPath, $finalName);
            $product->thumbnail = $uploadPath.$finalName;
        }

        if($product->save()){
            Session::put('msg','Added Success');
        }else{
            Session::put('msg','Fail to add');
        }
        return redirect(route('home'));
    }


    public function delete(){
        $id = request()->input('id');
        $product = Product::find($id);
        $pre_thumbnail = $product->thumbnail;
        if($pre_thumbnail){
            unlink($pre_thumbnail);
        }
        $product->delete();
        return response()->json([
            'status' => 'success',
        ]);
    }

    //Ajax call
    public function get_subproduct(){

        $subcategory_id = request()->input("subcategory_id");
        $this->data['filter_data'] = Product::where('subcategory_id', $subcategory_id)->get();
        $returnHTML = view('filter_data')->with($this->data)->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

}//ProductController
